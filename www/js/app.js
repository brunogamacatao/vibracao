var desenvolvimento = false;
var VIBRACAO_SERVER_BASE_URL = desenvolvimento ? "http://localhost:3000" : "http://enem.colegiomotiva.com.br";

var app = angular.module('vibracao', [
  'ionic', 
  'ngCordova', 
  'uiGmapgoogle-maps', 
  'vibracao.services', 
  'vibracao.rotinas.services',
  'vibracao.controllers', 
  'vibracao.rotinas.controllers',
  'vibracao.directives', 
  'vibracao.filters']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    document.addEventListener("pause", function() {
      var iframes = document.getElementsByTagName("iframe");
      if (iframes) {
        for (var i = 0; i < iframes.length; i++) {
          var iframe = iframes[i].contentWindow;
          iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
        }        
      }
    });

  });
});

app.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  // Turn off caching for demo simplicity's sake
  // $ionicConfigProvider.views.maxCache(0);

  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.navBar.alignTitle('center');
  $ionicConfigProvider.backButton.text('').icon('icon-voltar');
  $ionicConfigProvider.views.transition('platform');

  // Menu principal da aplicação
  $stateProvider.state('menu', {
    url: '/',
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  });

  // Estados relativos ao módulo 'Comentários'
  $stateProvider.state('comentarios', {
    url: '/comentarios',
    templateUrl: 'templates/comentarios/provas.html',
    controller: 'ComentariosProvasCtrl'
  });
  $stateProvider.state('comentarios_cores', {
    url: '/comentarios_cores',
    templateUrl: 'templates/comentarios/cores.html',
    controller: 'ComentariosCoresCtrl'
  });  
  $stateProvider.state('comentarios_questoes', {
    url: '/comentarios_questoes',
    templateUrl: 'templates/comentarios/questoes.html',
    controller: 'ComentariosQuestoesProvasCtrl'
  });
  $stateProvider.state('comentarios_redacao', {
    url: '/comentarios_redacao',
    templateUrl: 'templates/comentarios/redacao.html',
    controller: 'ComentariosRedacaoCtrl'
  });
  $stateProvider.state('comentarios_questao', {
    url: '/comentarios_questao',
    templateUrl: 'templates/comentarios/questao.html',
    controller: 'ComentariosQuestaoProvasCtrl'
  });

  // Estados relativos ao módulo 'Dicas'
  $stateProvider.state('dicas', {
    url: '/dicas',
    templateUrl: 'templates/dicas/index.html'
  });
  $stateProvider.state('dicas_dica', {
    url: '/dicas_dica/{area}',
    templateUrl: 'templates/dicas/dica.html',
    controller: 'DicaCtrl'
  });
  $stateProvider.state('dicas_detalhe', {
    url: '/dicas_detalhe',
    templateUrl: 'templates/dicas/detalhe.html',
    controller: 'DicaDetalheCtrl'
  });

  // Estados relativos ao módulo 'Instagram'
  $stateProvider.state('instagram', {
    url: '/instagram',
    templateUrl: 'templates/instagram/index.html',
    controller: 'InstagramCtrl'
  });
  $stateProvider.state('instagram_foto', {
    url: '/instagram_foto',
    templateUrl: 'templates/instagram/foto.html',
    controller: 'InstagramFotoCtrl'
  });

  // Estados relativos ao módulo 'RSS Notícias'
  $stateProvider.state('rss', {
    url: '/rss',
    templateUrl: 'templates/rss/index.html',
    controller: 'RSSCtrl'
  });
  $stateProvider.state('rss_noticia', {
    url: '/rss_noticia',
    templateUrl: 'templates/rss/noticia.html',
    controller: 'RSSNoticiaCtrl'
  });

  // Estados relativos ao módulo 'Facebook'
  $stateProvider.state('facebook', {
    url: '/facebook',
    templateUrl: 'templates/facebook/index.html',
    controller: 'FacebookCtrl'
  });

  // Estados relativos ao módulo 'Notícias'
  $stateProvider.state('noticias', {
    url: '/noticias',
    templateUrl: 'templates/noticias/index.html',
    controller: 'NoticiasCtrl'
  });
  $stateProvider.state('noticias_noticia', {
    url: '/noticias_noticia',
    templateUrl: 'templates/noticias/noticia.html',
    controller: 'NoticiasNoticiaCtrl'
  });

  // Estados relativos ao módulo 'Avisos'
  $stateProvider.state('avisos', {
    url: '/avisos',
    templateUrl: 'templates/avisos/index.html',
    controller: 'AvisosCtrl'
  });
  $stateProvider.state('avisos_aviso', {
    url: '/avisos_aviso',
    templateUrl: 'templates/avisos/aviso.html',
    controller: 'AvisosAvisoCtrl'
  });

  // Estados relativos ao módulo 'Videos'
  $stateProvider.state('videos', {
    url: '/videos',
    templateUrl: 'templates/videos/index.html',
    controller: 'VideosCtrl'
  });

  // Estados relativos ao módulo 'Fotos'
  $stateProvider.state('fotos', {
    url: '/fotos',
    templateUrl: 'templates/fotos/index.html',
    controller: 'FotosCtrl'
  });
  $stateProvider.state('fotos_foto', {
    url: '/fotos_foto',
    templateUrl: 'templates/fotos/foto.html',
    controller: 'FotosFotoCtrl'
  });

  // Estados relativos ao módulo 'Gabaritos'
  $stateProvider.state('gabaritos', {
    url: '/gabaritos',
    templateUrl: 'templates/gabaritos/index.html',
    controller: 'GabaritosCtrl'
  });
  $stateProvider.state('gabaritos_cores', {
    url: '/gabaritos_cores',
    templateUrl: 'templates/gabaritos/cores.html',
    controller: 'GabaritosCoresCtrl'
  });
  $stateProvider.state('gabaritos_questoes', {
    url: '/gabaritos_questoes',
    templateUrl: 'templates/gabaritos/questoes.html',
    controller: 'GabaritosQuestoesCtrl'
  });

  // Estado do módulo 'Colégio Motiva'
  $stateProvider.state('motiva', {
    url: '/motiva',
    templateUrl: 'templates/motiva/index.html',
    controller: 'ColegioCtrl'
  });

  // Estado do módulo 'Informações'
  $stateProvider.state('informacoes', {
    url: '/informacoes',
    templateUrl: 'templates/informacoes/index.html'
  });

  // Estados do módulo 'Rotinas de Estuddo'
  $stateProvider.state('rotinas', {
    url: '/rotinas',
    templateUrl: 'templates/rotinas/index.html',
    controller: 'RotinasCtrl'
  });

  // Rota padrão
  $urlRouterProvider.otherwise('/');  
});
