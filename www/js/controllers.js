var app = angular.module('vibracao.controllers', ['ionic', 'ngSanitize', 'ion-gallery']);

app.controller('AppCtrl', function($scope, $state, $rootScope, $ionicPlatform, $ionicPopup, $cordovaPush, $cordovaDialogs, Push) {
  var DEFAULT_MESSAGE_TITLE = "Vibração Motiva";

  $ionicPlatform.ready(function() {
    if (!window.cordova) return;

    // Push Notification Tentative
    if (ionic.Platform.isAndroid()) {
      var androidConfig = {
        "senderID": "40190666074"
      };

      $cordovaPush.register(androidConfig).then(function(result) {
        //$cordovaToast.showShortCenter('Registered for push notifications');
      }, function(error) {
        console.log("registration error: " + error);
      });
    } else if (ionic.Platform.isIOS()) {
      var iosConfig = {
        "badge": true,
        "sound": true,
        "alert": true,
      };

      $cordovaPush.register(iosConfig).then(function(deviceToken) {
        //$cordovaToast.showShortCenter('Registered for push notifications');
        $scope.deviceToken = deviceToken;
        Push.subscribe({platform: 'ios', token: deviceToken});
        console.log("[ios] deviceToken: " + deviceToken);
      }, function(error) {
        console.log("[ios] registration error: " + error);
      });
    } else {
      console.log("push notifications are not available on browsers");
    }
    // END - Push Notification Tentative
  });


  // Push Notification Tentative
  $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
    if (ionic.Platform.isAndroid()) { // START - notificationReceived - Android
      switch(notification.event) {
        case 'registered':
          var deviceToken = notification.regid;
          $scope.deviceToken = deviceToken;
          Push.subscribe({platform: 'android', token: deviceToken});
          console.log("[android] deviceToken: " + deviceToken);
          break;
        case 'message':
          $cordovaDialogs.alert(notification.message, DEFAULT_MESSAGE_TITLE);
          $state.go('avisos');
          break;
        case 'error':
          $cordovaDialogs.alert(notification.msg, DEFAULT_MESSAGE_TITLE);
          $state.go('avisos');
          break;
        default:
          $cordovaDialogs.alert(notification.event, DEFAULT_MESSAGE_TITLE);
          $state.go('avisos');
          break;
      }
      // END - notificationReceived - Android
    } else if (ionic.Platform.isIOS()) { // START - notificationReceived - IOS
      $state.go('avisos');

      if (parseInt(notification.foreground) === 1) { // App in foreground
        if (notification.body && notification.messageFrom) {
          $ionicPopup.alert({
            title: DEFAULT_MESSAGE_TITLE,
            template: notification.body
          });
        } else {
          $ionicPopup.alert({
            title: DEFAULT_MESSAGE_TITLE,
            template: notification.alert
          });
        }
      } else { // App in background
        if (notification.body && notification.messageFrom) {
          $cordovaDialogs.alert(notification.body, DEFAULT_MESSAGE_TITLE);
        } else {
          $cordovaDialogs.alert(notification.alert, DEFAULT_MESSAGE_TITLE);
        }
      }
    } // END - notificationReceived - IOS
  });
  // END - Push Notification Tentative

});

app.controller('ComentariosProvasCtrl', function($scope, $state, $ionicLoading, $cordovaDialogs, ComentariosService, VibracaoSession) {
  $scope.provas = [];
  var cores  = ['energized', 'positive', 'assertive'];

  $scope.atualiza = function() {
    $ionicLoading.show({template: 'Carregando ...'});
    ComentariosService.getProvas().then(function(provas) {
      $scope.provas = provas;
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
    }, function(mensagem_erro) {
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      console.log(mensagem_erro);
      $cordovaDialogs.alert(mensagem_erro, "Vibração Motiva");
      $state.go('menu');
    });

  };

  $scope.cor = function(index) {
    return cores[index % cores.length];
  };

  $scope.selecionaProva = function(prova) {
    VibracaoSession.put('prova_selecionada', prova);

    if (prova.redacao) {
      $state.go('comentarios_redacao');
    } else {
      $state.go('comentarios_cores');
    }
  };

  $scope.atualiza();
});

app.controller('ComentariosCoresCtrl', function($scope, $state, $cordovaDialogs, CoresService, VibracaoSession) {
  $scope.cores = [];
  $scope.prova = VibracaoSession.get('prova_selecionada');

  CoresService.getCores().then(function(cores) {
    $scope.cores = cores;
  }, function(mensagem_erro) {
    console.log(mensagem_erro);
    $cordovaDialogs.alert(mensagem_erro, "Vibração Motiva");
    $state.go('menu');
  });

  $scope.selecionaCor = function(cor) {
    VibracaoSession.put('cor_selecionada', cor);
    $state.go('comentarios_questoes');
  };
});

app.controller('ComentariosQuestoesProvasCtrl', function($scope, $state, ComentariosService, VibracaoSession) {
  var questoes = [];
  var cor = VibracaoSession.get('cor_selecionada');

  function initData() {
    $scope.prova = VibracaoSession.get('prova_selecionada');

    if (!$scope.prova) {
      $state.go('menu');
    }

    var i;
    for (i = 0; i < 181; i++) {
      questoes[i] = false;
    }

    for (i = 0; i < $scope.prova.questoes.length; i++) {
      var questao = $scope.prova.questoes[i];
      var numero  = questao['numero_' + cor.nome.toLowerCase()];
      if (numero) {
        questoes[numero] = true;
      }
    }    
  }

  $scope.corrigida = function(index) {
    return questoes[index] ? 'questao-corrigida' : 'questao-nao-corrigida';
  };

  $scope.atualiza = function() {
    ComentariosService.getProvas().then(function(provas) {
      for (var i = 0; i < provas.length; i++) {
        if (provas[i].id == $scope.prova.id) {
          $scope.prova = provas[i];
        }
      }
      initData();
      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  $scope.selecionaQuestao = function(index) {
    if (questoes[index]) {
      var questao;

      for (var i = 0; i < $scope.prova.questoes.length; i++) {
        questao = $scope.prova.questoes[i];
        if (questao.numero == index) {
          break;
        }
      }

      VibracaoSession.put('questao_selecionada', questao);
      $state.go('comentarios_questao');      
    }
  };

  initData();
});

app.controller('ComentariosQuestaoProvasCtrl', function($scope, $state, $ionicModal, VibracaoSession) {
  $scope.questao = VibracaoSession.get('questao_selecionada');

  if (!$scope.questao) {
    $state.go('menu');
  }

  $scope.imagemUrl = function(imagem) {
    return VIBRACAO_SERVER_BASE_URL + imagem.imagem.imagem.url;
  };

  $scope.imagemDefaultUrl = function() {
    if ($scope.questao.imagens && $scope.questao.imagens.length > 0) {
      return $scope.imagemUrl($scope.questao.imagens[0]);
    }

    return null;
  };

  $scope.mostraVideo = function() {
    // Criação da janela modal para exibir os videos
    $ionicModal.fromTemplateUrl('templates/modal/video.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalVideo = modal;
      $scope.modalVideo.show();
    });
  };

  $scope.fechaModal = function() {
    $scope.modalVideo.hide();
    $scope.modalVideo.remove();
    $scope.modalVideo = null;
  };

  //Remoção da janela modal da memória quando essa view for destruída
  $scope.$on('$destroy', function() {
    if ($scope.modalVideo) {
      $scope.modalVideo.remove();
    }
  });
});

app.controller('ComentariosRedacaoCtrl', function($scope, $state, $ionicModal, VibracaoSession) {
  $scope.prova = VibracaoSession.get('prova_selecionada');
  $scope.questao = $scope.prova;

  $scope.imagemUrl = function() {
    return VIBRACAO_SERVER_BASE_URL + $scope.prova.imagem.imagem.url;
  };

  $scope.mostraVideo = function() {
    // Criação da janela modal para exibir os videos
    $ionicModal.fromTemplateUrl('templates/modal/video.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalVideo = modal;
      $scope.modalVideo.show();
    });
  };

  $scope.fechaModal = function() {
    $scope.modalVideo.hide();
    $scope.modalVideo.remove();
    $scope.modalVideo = null;
  };

  //Remoção da janela modal da memória quando essa view for destruída
  $scope.$on('$destroy', function() {
    if ($scope.modalVideo) {
      $scope.modalVideo.remove();
    }
  });
});

app.controller('InstagramCtrl', function($scope, $ionicLoading, $timeout, $state, Instagram, VibracaoSession) {
  $scope.items = [];
  $scope.newItems = [];
  $scope.noMoreItemsAvailable = false;

  $ionicLoading.show({template: 'Carregando ...'});
  Instagram.GetFeed().then(function(items) {
    $scope.items = items.concat($scope.items);
    $ionicLoading.hide();
  });


  $scope.doRefresh = function() {
    if ($scope.newItems.length > 0) {
      $scope.items = $scope.newItems.concat($scope.items);

      //Stop the ion-refresher from spinning
      $scope.$broadcast('scroll.refreshComplete');

      $scope.newItems = [];
    } else {
      $ionicLoading.show({template: 'Carregando ...'});
      Instagram.GetNewPhotos().then(function(items) {
        $scope.items = items.concat($scope.items);

        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
        $ionicLoading.hide();
      });
    }
  };  

  $scope.loadMore = function() {
    $ionicLoading.show({template: 'Carregando ...'});
    Instagram.GetOldPhotos().then(function(items) {
      $ionicLoading.hide();
      $scope.items = $scope.items.concat(items);

      //Tell the infinite scroll component that the loading is complete
      $scope.$broadcast('scroll.infiniteScrollComplete');
      
      // an empty array indicates that there are no more items
      if (items.length === 0) {
        $scope.noMoreItemsAvailable = true;
      }
    });    
  };

  $scope.exibeFoto = function(foto) {
    VibracaoSession.put('foto_selecionada', foto);
    $state.go('instagram_foto');
  };
});

app.controller('InstagramFotoCtrl', function($scope, VibracaoSession) {
  $scope.item = VibracaoSession.get('foto_selecionada');
});

app.controller('RSSCtrl', function($scope, $ionicLoading, $state, FeedService, VibracaoSession) {
  var cores  = ['energized', 'positive', 'assertive'];
  var rssUrl = 'http://rss.uol.com.br/feed/educacao.xml';

  $scope.mostraNoticia = function(noticia) {
    VibracaoSession.put('noticia_selecionada', noticia);
    $state.go('rss_noticia');
    console.log('mostraNoticia');
    console.log(noticia);
  };

  $scope.atualizaNoticias = function() {
    $scope.entries = [];
    $ionicLoading.show({template: 'Carregando ...'});

    FeedService.getEntries(rssUrl).then(function(noticias) {
      $ionicLoading.hide();
      $scope.noticias = noticias;
      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  $scope.cor = function(index) {
    return cores[index % cores.length];
  };

  $scope.atualizaNoticias();
});

app.controller('RSSNoticiaCtrl', function($scope, $sce, VibracaoSession) {
  $scope.noticia = VibracaoSession.get('noticia_selecionada');

  if (!$scope.noticia) {
    $scope.noticia = {
      // just a placeholder for tests
      link: 'http://vibracao.colegiomotiva.com.br/comentario-de-matematica-enem-2014-2o-dia'
    };
  }

  $scope.urlNoticia = function() {
    return $sce.trustAsResourceUrl($scope.noticia.link);
  };
});

app.controller('FacebookCtrl', function($scope, $ionicLoading, FacebookService) {
  $scope.posts = [];

  $scope.atualiza = function() {
    $ionicLoading.show({template: 'Carregando ...'});

    FacebookService.getPosts().then(function(posts) {
      $scope.posts = posts;
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  $scope.atualiza();
});

// Controladores do modulo Noticias
app.controller('NoticiasCtrl', function($scope, $ionicLoading, $state, $cordovaDialogs, NoticiasService, VibracaoSession) {
  var cores  = ['energized', 'positive', 'assertive'];

  $scope.mostraNoticia = function(noticia) {
    VibracaoSession.put('noticia_selecionada', noticia);
    $state.go('noticias_noticia');
  };

  $scope.atualizaNoticias = function() {
    $ionicLoading.show({template: 'Carregando ...'});

    NoticiasService.getNoticias().then(function(noticias) {
      $ionicLoading.hide();
      $scope.noticias = noticias;
      $scope.$broadcast('scroll.refreshComplete');
    }, function(mensagem_erro) {
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      console.log(mensagem_erro);
      $cordovaDialogs.alert(mensagem_erro, "Vibração Motiva");
      $state.go('menu');
    });
  };

  $scope.cor = function(index) {
    return cores[index % cores.length];
  };

  $scope.atualizaNoticias();
});

app.controller('NoticiasNoticiaCtrl', function($scope, $state, $sce, $cordovaSocialSharing, $ionicModal, VibracaoSession) {
  $scope.noticia = VibracaoSession.get('noticia_selecionada');

  if (!$scope.noticia) {
    $state.go('menu');
  }

  $scope.imgUrl = function() {
    return VIBRACAO_SERVER_BASE_URL + $scope.noticia.imagem.imagem.url;
  };

  $scope.mostraVideo = function() {
    $scope.questao = $scope.noticia;
    // Criação da janela modal para exibir os videos
    $ionicModal.fromTemplateUrl('templates/modal/video.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalVideo = modal;
      $scope.modalVideo.show();
    });
  };

  $scope.fechaModal = function() {
    $scope.modalVideo.hide();
    $scope.modalVideo.remove();
    $scope.modalVideo = null;
  };

  //Remoção da janela modal da memória quando essa view for destruída
  $scope.$on('$destroy', function() {
    if ($scope.modalVideo) {
      $scope.modalVideo.remove();
    }
  });
});

// Controladores do modulo Avisos
app.controller('AvisosCtrl', function($scope, $ionicLoading, $state, $cordovaDialogs, AvisosService, VibracaoSession) {
  var cores  = ['energized', 'positive', 'assertive'];

  $scope.mostraAviso = function(aviso) {
    VibracaoSession.put('aviso_selecionado', aviso);
    $state.go('avisos_aviso');
  };

  $scope.atualizaAvisos = function() {
    $ionicLoading.show({template: 'Carregando ...'});

    AvisosService.getAvisos().then(function(avisos) {
      $ionicLoading.hide();
      $scope.avisos = avisos;
      $scope.$broadcast('scroll.refreshComplete');
    }, function(mensagem_erro) {
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      console.log(mensagem_erro);
      $cordovaDialogs.alert(mensagem_erro, "Vibração Motiva");
      $state.go('menu');
    });
  };

  $scope.cor = function(index) {
    return cores[index % cores.length];
  };

  $scope.atualizaAvisos();
});

app.controller('AvisosAvisoCtrl', function($scope, $state, $sce, $ionicModal, VibracaoSession) {
  $scope.aviso = VibracaoSession.get('aviso_selecionado');

  if (!$scope.aviso) {
    $state.go('menu');
  }

  $scope.imgUrl = function() {
    return VIBRACAO_SERVER_BASE_URL + $scope.aviso.imagem.imagem.url;
  };

  $scope.mostraVideo = function() {
    $scope.questao = $scope.aviso;
    // Criação da janela modal para exibir os videos
    $ionicModal.fromTemplateUrl('templates/modal/video.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalVideo = modal;
      $scope.modalVideo.show();
    });
  };

  $scope.fechaModal = function() {
    $scope.modalVideo.hide();
    $scope.modalVideo.remove();
    $scope.modalVideo = null;
  };

  //Remoção da janela modal da memória quando essa view for destruída
  $scope.$on('$destroy', function() {
    if ($scope.modalVideo) {
      $scope.modalVideo.remove();
    }
  });
});

// Controladores do modulo Videos
app.controller('VideosCtrl', function($scope, $ionicLoading, $state, VideosService) {
  $scope.videosParaMostrar = 5;
  $scope.temMais = true;

  $scope.atualizaVideos = function() {
    $ionicLoading.show({template: 'Carregando ...'});

    VideosService.getVideos().then(function(videos) {
      $ionicLoading.hide();
      $scope.videos = videos;
      $scope.temMais = $scope.videos.length > $scope.videosParaMostrar;
      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  $scope.mostrarMaisVideos = function() {
    $scope.videosParaMostrar += 5;
    $scope.temMais = $scope.videos.length > $scope.videosParaMostrar;
    $scope.$broadcast('scroll.infiniteScrollComplete');
  };

  $scope.atualizaVideos();
});

// Controladores do modulo Fotos
app.controller('FotosCtrl', function($scope, $ionicLoading, $state, $ionicModal, $cordovaSocialSharing, FotosService, VibracaoSession) {
  $scope.fotos = [];

  $scope.atualizaFotos = function() {
    $ionicLoading.show({template: 'Carregando ...'});

    FotosService.getFotos().then(function(fotos) {
      $ionicLoading.hide();
      $scope.fotos = fotos;
      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  $scope.iconeUrl = function(foto) {
    return VIBRACAO_SERVER_BASE_URL + foto.imagem.imagem.thumbnail_128.url;
  };

  $scope.exibeFoto = function(index) {
    $scope.selectedIndex = index;

    $ionicModal.fromTemplateUrl('templates/fotos/foto.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalFoto = modal;
      $scope.modalFoto.show();
    });
  };

  $scope.fechaModal = function() {
    $scope.modalFoto.hide();
    $scope.modalFoto.remove();
    $scope.modalFoto = null;
  };

  $scope.foto = function() {
    return $scope.fotos[$scope.selectedIndex];
  };

  $scope.urlFoto = function(index) {
    return VIBRACAO_SERVER_BASE_URL + $scope.fotos[index].imagem.imagem.url;
  };

  $scope.atualizaFotos();
});

app.controller('FotosFotoCtrl', function($scope, VibracaoSession) {
  $scope.foto = VibracaoSession.get('foto_selecionada');

  $scope.urlFoto = function() {
    return VIBRACAO_SERVER_BASE_URL + $scope.foto.imagem.imagem.url;
  };
});

// Controladores do modulo Dicas
app.controller('DicaCtrl', function($scope, $ionicLoading, $state, $stateParams, $cordovaDialogs, DicasService, VibracaoSession) {
  var cores  = ['energized', 'positive', 'assertive'];

  $scope.atualizaDicas = function() {
    $ionicLoading.show({template: 'Carregando ...'});

    DicasService.getDicas($stateParams.area).then(function(dicas) {
      $ionicLoading.hide();
      $scope.dicas = dicas;
      $scope.$broadcast('scroll.refreshComplete');
    }, function(mensagem_erro) {
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      console.log(mensagem_erro);
      $cordovaDialogs.alert(mensagem_erro, "Vibração Motiva");
      $state.go('menu');
    });
  };

  $scope.exibeDica = function(dica) {
    VibracaoSession.put('dica_selecionada', dica);
    $state.go('dicas_detalhe');
  };

  $scope.cor = function(index) {
    return cores[index % cores.length];
  };

  $scope.atualizaDicas();
});

app.controller('DicaDetalheCtrl', function($scope, $ionicLoading, $stateParams, $ionicModal, VibracaoSession) {
  $scope.dica = VibracaoSession.get('dica_selecionada');
  $scope.questao = $scope.dica;

  $scope.imgUrl = function() {
    return VIBRACAO_SERVER_BASE_URL + $scope.dica.imagem.imagem.url;
  };

  $scope.mostraVideo = function() {
    // Criação da janela modal para exibir os videos
    $ionicModal.fromTemplateUrl('templates/modal/video.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalVideo = modal;
      $scope.modalVideo.show();
    });
  };

  $scope.fechaModal = function() {
    $scope.modalVideo.hide();
    $scope.modalVideo.remove();
    $scope.modalVideo = null;
  };

  //Remoção da janela modal da memória quando essa view for destruída
  $scope.$on('$destroy', function() {
    if ($scope.modalVideo) {
      $scope.modalVideo.remove();
    }
  });  
});

app.controller('GabaritosCtrl', function($scope, $state, $ionicLoading, $cordovaDialogs, ComentariosService, VibracaoSession) {
  $scope.provas = [];
  var cores  = ['energized', 'positive', 'assertive'];

  $scope.atualiza = function() {
    $ionicLoading.show({template: 'Carregando ...'});
    ComentariosService.getProvas().then(function(provas) {
      $scope.provas = [];
      for (var i = 0; i < provas.length; i++) {
        if (!provas[i].redacao) {
          $scope.provas.push(provas[i]);
        }
      }
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
    }, function(mensagem_erro) {
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      console.log(mensagem_erro);
      $cordovaDialogs.alert(mensagem_erro, "Vibração Motiva");
      $state.go('menu');
    });
  };

  $scope.cor = function(index) {
    return cores[index % cores.length];
  };

  $scope.selecionaProva = function(prova) {
    VibracaoSession.put('prova_selecionada', prova);
    $state.go('gabaritos_cores');
  };

  $scope.atualiza();
});

app.controller('GabaritosCoresCtrl', function($scope, $state, $cordovaDialogs, CoresService, VibracaoSession) {
  $scope.cores = [];
  $scope.prova = VibracaoSession.get('prova_selecionada');

  CoresService.getCores().then(function(cores) {
    $scope.cores = cores;
  }, function(mensagem_erro) {
    console.log(mensagem_erro);
    $cordovaDialogs.alert(mensagem_erro, "Vibração Motiva");
    $state.go('menu');
  });

  $scope.selecionaCor = function(cor) {
    VibracaoSession.put('cor_selecionada', cor);
    $state.go('gabaritos_questoes');
  };
});

app.controller('GabaritosQuestoesCtrl', function($scope, $state, $ionicLoading, $cordovaDialogs, GabaritosService, VibracaoSession) {
  $scope.prova    = VibracaoSession.get('prova_selecionada');
  $scope.cor      = VibracaoSession.get('cor_selecionada');
  $scope.gabarito = [];
  $scope.pronto   = false;

  function indiceDaPrimeiraQuestao() {
    return ($scope.prova.numero_primeira_questao - 1);
  };

  function resposta(numero) {
    for (var i = 0; i < $scope.gabarito.length; i++) {
      var questao = $scope.gabarito[i];
      if (questao.numero_questao == numero) {
        return questao.resposta;
      }
    }    
    return "";
  };

  $scope.numeroQuestao = function(x, y) {
    return (x - 1) * 5 + y + indiceDaPrimeiraQuestao();
  };

  $scope.respostaQuestao = function(x, y) {
    return resposta((x - 1) * 5 + y + indiceDaPrimeiraQuestao());
  };

  $scope.atualizaDicas = function() {
    $ionicLoading.show({template: 'Carregando ...'});
    $scope.pronto = false;

    GabaritosService.getGabarito($scope.prova.id, $scope.cor.id).then(function(gabarito) {
      $ionicLoading.hide();
      $scope.gabarito = gabarito;
      $scope.pronto   = true;
      $scope.$broadcast('scroll.refreshComplete');
    }, function(mensagem_erro) {
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      console.log(mensagem_erro);
      $cordovaDialogs.alert(mensagem_erro, "Vibração Motiva");
      $state.go('menu');
    });

  };

  $scope.corQuestao = function() {
    return "questao-" + $scope.cor.nome.toUpperCase();
  };

  $scope.atualizaDicas();
});

app.controller('ColegioCtrl', function($scope) {
  $scope.unidades = [
    {nome: 'Jardim Ambiental',
     mapa: {center: {latitude: '-7.23834', longitude: '-35.88146'}, zoom: 17},
     endereco: 'Rua Luiza Bezerra Motta, 589 - Catolé Campina Grande - Paraíba CEP: 58104-600',
     telefone: '(83) 2101-4900',
     twitter: 'motivacg'
    },
    {nome: 'Motiva Centro',
     mapa: {center: {latitude: '-7.22284', longitude: '-35.88423'}, zoom: 17},
     endereco: 'Rua Irineu Joffily, 163- Centro Campina Grande - Paraíba CEP: 58101-030',
     telefone: '(83) 2101-4800',
     twitter: 'motivacg'
    },
    {nome: 'Motiva Ambiental',
     mapa: {center: {latitude: '-7.11349', longitude: '-34.82818'}, zoom: 17},
     endereco: 'Rua Silvino Lopes, 255 - Tambaú João Pessoa- Paraíba CEP: 58039-190',
     telefone: '(83) 3015-2100',
     twitter: 'motivajp'
    },
    {nome: 'Motiva Miramar',
     mapa: {center: {latitude: '-7.11675', longitude: '-34.83719'}, zoom: 17},
     endereco: 'Av. Rui Carneiro, 850 - Miramar João Pessoa- Paraíba CEP 58032-090',
     telefone: '(83) 3015-2800',
     twitter: 'motivajp'
    }
  ];
});