var app = angular.module('vibracao.directives', []);

app.directive('botaoMenu', function() {
  return {
      restrict: 'E',
      replace: 'true',
      templateUrl: 'templates/directives/botao_menu.html',
      scope: {
        icone: "@icone",
        texto: "@texto",
        url:   "@url"
      }
  };
});

app.directive('verticalCenter', function($window){
  // vertical center element on page
  return function(scope, element) {
    var pixels;
    var w = angular.element($window);

    function setPixels(){
      var h  = $window.innerHeight;
      var eh = element[0].clientHeight;

      pixels = (h - eh) / 2;

      if (pixels < 50) { 
        pixels = 50 + 'px'; 
      } else {
        pixels = pixels + 'px';
      }

      element.css('margin-top', pixels);
    }

    setPixels();

    w.bind('resize', function(){
      setPixels();
    });
  }
});

app.directive('standardTimeNoMeridian', function () {
  return {
    restrict: 'AE',
    replace: true,
    scope: {
      etime: '=etime'
    },
    template: "<strong>{{stime}}</strong>",
    link: function (scope, elem, attrs) {
      scope.stime = epochParser(scope.etime, 'time');

      function prependZero(param) {
        if (String(param).length < 2) {
          return "0" + String(param);
        }
        return param;
      }

      function epochParser(val, opType) {
        if (val === null) {
          return "00:00";
        } else {
          if (opType === 'time') {
            var hours = parseInt(val / 3600);
            var minutes = (val / 60) % 60;

            return (prependZero(hours) + ":" + prependZero(minutes));
          }
        }
      }

      scope.$watch('etime', function (newValue, oldValue) {
        scope.stime = epochParser(scope.etime, 'time');
      });
    }
  };
});

app.directive('socialLinks', function() {
  return {
      restrict: 'E',
      replace: 'true',
      templateUrl: 'templates/directives/social_links.html',
      scope: {
        titulo: "@titulo",
        texto:  "@texto",
        imagem: "@imagem",
        url:    "@url"},
      controller: function($scope, $cordovaSocialSharing, $ionicLoading) {
        $scope.shareViaFacebook = function() {
          $ionicLoading.show({template: 'Aguarde ...'});
          $cordovaSocialSharing.shareViaFacebook($scope.texto, $scope.imagem, $scope.url).then(function(result) {
            $ionicLoading.hide();
          }, function(err) {
            $ionicLoading.hide();
          });
        };
        $scope.shareViaTwitter = function() {
          $ionicLoading.show({template: 'Aguarde ...'});
          $cordovaSocialSharing.shareViaTwitter($scope.texto, $scope.imagem, $scope.url).then(function(result) {
            $ionicLoading.hide();
          }, function(err) {
            $ionicLoading.hide();
          });
        };
        $scope.shareViaWhatsapp = function() {
          $ionicLoading.show({template: 'Aguarde ...'});
          $cordovaSocialSharing.shareViaWhatsApp($scope.texto, $scope.imagem, $scope.url).then(function(result) {
            $ionicLoading.hide();
          }, function(err) {
            $ionicLoading.hide();
          });
        };
        $scope.shareAll = function() {
          $ionicLoading.show({template: 'Aguarde ...'});
          $cordovaSocialSharing.share($scope.texto, $scope.titulo, $scope.imagem, $scope.url).then(function(result) {
            $ionicLoading.hide();
          }, function(err) {
            $ionicLoading.hide();
          });
        };
      }
  };  
});