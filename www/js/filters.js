var app = angular.module('vibracao.filters', []);

app.filter('youtubeEmbedUrl', function ($sce) {
  return function(videoId) {
    return $sce.trustAsResourceUrl('http://www.youtube.com/embed/' + videoId + '?enablejsapi=1&rel=0');
  };
});

app.filter('data', function() {
  var MESES = ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'];

  function pad2(number) {  
   return (number < 10 ? '0' : '') + number
  }

  return function(dataStr) {
    if (!dataStr) return dataStr;
    
    dataStr = dataStr.replace("+0000", ".000+00:00");
    var d = new Date(Date.parse(dataStr));

    var dia    = d.getDate();    // 1-31
    var mes    = d.getMonth();   // 0-11
    var hora   = d.getHours();   // 0-23
    var minuto = d.getMinutes(); //0-59

    return dia + " de " + MESES[mes] + " às " + hora + ":" + pad2(minuto);
  };
});