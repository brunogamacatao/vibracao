var app = angular.module('vibracao.rotinas.controllers', ['ionic', 'ionic-timepicker', 'ionic-datepicker']);

app.controller('RotinasCtrl', function($scope, $ionicPlatform, $ionicModal, RotinasService) {
  $scope.shouldShowDelete = false;
  $scope.alarms = RotinasService.getAlarms();

  $scope.alarme = {
    periodicidade: 0,
    nome: 'Alarme'
  };

  $scope.timePickerObject24Hour = {
    inputEpochTime: ((new Date()).getHours() * 60 * 60),  //Optional
    step: 1,  //Optional
    format: 24,  //Optional
    titleLabel: 'Hora',  //Optional
    closeLabel: 'Cancela',  //Optional
    setLabel: 'Seleciona',  //Optional
    setButtonType: 'button-assertive',  //Optional
    closeButtonType: 'button-light',  //Optional
    callback: function (val) {    //Mandatory
      timePicker24Callback(val);
    }
  };

  function timePicker24Callback(val) {
    if (typeof (val) === 'undefined') {
      console.log('Time not selected');
    } else {
      $scope.timePickerObject24Hour.inputEpochTime = val;
      var selectedTime = new Date(val * 1000);
      console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), ':', selectedTime.getUTCMinutes(), 'in UTC');
    }
  }  

  var monthList = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Aug", "Set", "Out", "Nov", "Dez"];
  var weekDaysList = ["D", "S", "T", "Q", "Q", "S", "S"];
  $scope.datepickerObject = {};
  $scope.datepickerObject.inputDate = new Date();

  $scope.datepickerObjectPopup = {
    titleLabel: 'Data', //Optional
    todayLabel: 'Hoje', //Optional
    closeLabel: 'X', //Optional
    setLabel: 'OK', //Optional
    errorMsgLabel : 'Selecione um horário', //Optional
    setButtonType : 'button-assertive', //Optional
    modalHeaderColor:'bar-positive', //Optional
    modalFooterColor:'bar-positive', //Optional
    templateType:'popup', //Optional
    inputDate: $scope.datepickerObject.inputDate, //Optional
    mondayFirst: true, //Optional
    weekDaysList: weekDaysList,
    monthList:monthList, //Optional
    callback: function (val) { //Optional
      datePickerCallbackPopup(val);
    }
  };  

  var datePickerCallbackPopup = function (val) {
    if (typeof(val) === 'undefined') {
      console.log('No date selected');
    } else {
      $scope.datepickerObjectPopup.inputDate = val;
      console.log('Selected date is : ', val)
    }
  };

  $scope.exibeModalNovaAtividade = function() {
    // Criação da janela modal para criar uma nova atividade
    $ionicModal.fromTemplateUrl('templates/rotinas/modal_atividade.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalNovaAtividade = modal;
      $scope.modalNovaAtividade.show();
    });
  };

  $scope.fechaModalNovaAtividade = function() {
    $scope.modalNovaAtividade.hide();
    $scope.modalNovaAtividade.remove();
    $scope.modalNovaAtividade = null;
  };

  //Remoção da janela modal da memória quando essa view for destruída
  $scope.$on('$destroy', function() {
    if ($scope.modalNovaAtividade) {
      $scope.modalNovaAtividade.remove();
    }
  });

  $scope.adicionarAtividade = function() {
    var dataInicio = new Date($scope.datepickerObjectPopup.inputDate.getTime());
    var horaInicio = new Date($scope.timePickerObject24Hour.inputEpochTime * 1000);

    var hora = horaInicio.getHours() + 3;
    if (hora > 24) {
      hora -= 24;
    }

    dataInicio.setHours(hora);
    dataInicio.setMinutes(horaInicio.getMinutes());
    dataInicio.setSeconds(0);

    console.log(dataInicio);

    var periodicidade = [0, 'hour', 'day', 'week', 'month'];

    var alarme = {
      title: 'Vibração Motiva',
      text: $scope.alarme.nome,
      at: dataInicio,
      ativo: true
    };

    if ($scope.alarme.periodicidade > 0) {
      alarme.every = periodicidade[$scope.alarme.periodicidade];
    }

    RotinasService.addAlarm(alarme);
    $scope.alarms = RotinasService.getAlarms();
    $scope.fechaModalNovaAtividade();
  };

  $scope.excluirAtividade = function(alarm) {
    RotinasService.removeAlarm(alarm.id);
    $scope.alarms = RotinasService.getAlarms();
  };

  $scope.limpaTudo = function() {
    RotinasService.reset();
    $scope.alarms = RotinasService.getAlarms();
    $scope.fechaModalNovaAtividade();
  };

  $scope.toggleShowDelete = function() {
    $scope.shouldShowDelete = !$scope.shouldShowDelete;
  };

  $scope.toggleAtivo = function(indice) {
    if ($scope.alarms[indice].ativo) {
      RotinasService.enable($scope.alarms[indice]);
    } else {
      RotinasService.disable($scope.alarms[indice]);
    }
    $scope.alarms = RotinasService.getAlarms();
  };
});
