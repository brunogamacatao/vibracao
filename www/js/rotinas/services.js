var services = angular.module('vibracao.rotinas.services', []);

services.factory('RotinasService', function($http, $window, $cordovaLocalNotification) {
  // Constants
  var ALARMS_STORAGE_KEY = 'alarmes_rotina';

  // Private Properties
  var myAlarms = [];

  // Private Methods
  var loadLocalAlarms = function() {
    if ($window.localStorage[ALARMS_STORAGE_KEY]) {
      myAlarms = JSON.parse($window.localStorage[ALARMS_STORAGE_KEY]);
    }
  };

  var saveLocalAlarms = function() {
    $window.localStorage[ALARMS_STORAGE_KEY] = JSON.stringify(myAlarms);
  };

  var addLocalAlarm = function(alarm) {
    if (!alarm.id) {
      alarm.id = getNextId();
    }

    myAlarms.push(alarm);
  };

  var addNativeAlarm = function(alarm) {
    if (window.cordova) {

      var nativeAlarm = {
        id: alarm.id,
        title: alarm.title,
        text: alarm.text,
        at: alarm.at
      };

      if (alarm.every) {
        nativeAlarm.every = alarm.every;
      }

      console.log('Registrando o alarme:');
      console.log(nativeAlarm);
      console.log('Para comparacao, data atual:');
      console.log(new Date());

      $cordovaLocalNotification.schedule(nativeAlarm);
    } else {
      console.log('Essa funcionalidade não está disponível no browser')
    }    
  };

  var removeLocalAlarm = function(id) {
    for (var i = 0; i < myAlarms.length; i++) {
      if (myAlarms[i].id === id) {
        myAlarms.splice(i, 1);
        break;
      }
    }
  };

  var removeNativeAlarm = function(id) {
    if (window.cordova) {
      $cordovaLocalNotification.cancel(id);
    } else {
      console.log('Essa funcionalidade não está disponível no browser')
    }    
  };

  var getNextId = function() {
    return myAlarms.length + 1;
  };

  var init = function() {
    loadLocalAlarms();
  };

  // Execute the init method at Service load
  init();

  // Service's public interface
  return {
    getAlarms: function() {
      return myAlarms;
    },
    addAlarm: function(alarm) {
      addLocalAlarm(alarm);
      addNativeAlarm(alarm);
      saveLocalAlarms();

      return alarm.id;
    },
    removeAlarm: function(id) {
      removeLocalAlarm(id);
      removeNativeAlarm(id);
      saveLocalAlarms();
    },
    reset: function() {
      myAlarms = [];
      saveLocalAlarms();

      if (window.cordova) {
        $cordovaLocalNotification.cancelAll();
      } else {
        console.log('Essa funcionalidade não está disponível no browser')
      }    
    },
    enable: function(alarm) {
      addNativeAlarm(alarm);
      saveLocalAlarms();
    },
    disable: function(alarm) {
      removeNativeAlarm(alarm.id);
      saveLocalAlarms();
    }
  };
});