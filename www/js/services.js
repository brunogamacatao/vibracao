var app = angular.module('vibracao.services', ['ionic', 'ngResource']);

app.factory('LocalStorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    hasKey: function(key) {
      return ($window.localStorage[key]) ? true : false;
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}]);

app.factory('ConnectivityMonitor', function($rootScope, $cordovaNetwork){
  return {
    isOnline: function(){
      if(ionic.Platform.isWebView()){
        return $cordovaNetwork.isOnline();    
      } else {
        return navigator.onLine;
      }
    },
    isOffline: function(){
      if(ionic.Platform.isWebView()){
        return !$cordovaNetwork.isOnline();    
      } else {
        return !navigator.onLine;
      }
    },
    startWatching: function(){
      if(ionic.Platform.isWebView()){

        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
          console.log("went online");
        });

        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
          console.log("went offline");
        });

      } else {
        window.addEventListener("online", function(e) {
          console.log("went online");
        }, false);    

        window.addEventListener("offline", function(e) {
          console.log("went offline");
        }, false);  
      }       
    }
  }
});

app.factory('VibracaoSession', function() {
  var sessionObj = {};

  sessionObj.session = {};

  sessionObj.put = function(key, val) {
    sessionObj.session[key] = val;
  };

  sessionObj.get = function(key) {
    return sessionObj.session[key];
  };

  return sessionObj;
});

app.factory('Instagram', function($http, $q) {
  var BASE_URL = VIBRACAO_SERVER_BASE_URL + "/instagram/index";
  var items    = [];
  var nextUrl  = 0;  // next max id - for fetching older photos
  var NewInsta = 0;  // min id - for fetching newer photos

  return {
    GetFeed: function() {
      return $http.get(BASE_URL).then(function(response) {
        items    = response.data.data;
        nextUrl  = response.data.pagination.next_max_id;
        NewInsta = response.data.pagination.min_id;

        return items;
      });        
    },

    GetNewPhotos: function() {
      return $http.get(BASE_URL + '?min_id=' + NewInsta).then(function(response) {
        items = response.data.data;

        if(response.data.data.length > 0){
          NewInsta = response.data.pagination.min_id;
        }

        return items;
      });
    },
    
    /**
     * Always returns a promise object. If there is a nextUrl, 
     * the promise object will resolve with new instragram results, 
     * otherwise it will always be resolved with [].
     **/
    GetOldPhotos: function() {
      if (typeof nextUrl != "undefined") {
        return $http.get(BASE_URL + '?max_id=' + nextUrl).then(function(response) {
          if(response.data.data.length > 0){
            nextUrl = response.data.pagination.next_max_id;
          }
          
          items = response.data.data;

          return items;
        });
      } else {
        var deferred = $q.defer();
        deferred.resolve([]);
        return deferred.promise;
      }
    }
  }

});

app.factory('FacebookService', function($http) {
  return {
    getPosts: function() {
      return $http.get(VIBRACAO_SERVER_BASE_URL + '/facebook/index').then(function(response) {
        return response.data;
      });           
    }
  };  
});

app.factory('FeedService', function($http, $q) {
  var entries;

  return {
    getEntries: function(url) {
      var deferred = $q.defer();

      google.load("feeds", "1", {callback:function() {
        console.log('googles init called');
        var feed = new google.feeds.Feed(url);

        feed.setNumEntries(100);

        feed.load(function(result) {
          entries = result.feed.entries;
          deferred.resolve(entries);
        });
      }});

      return deferred.promise;
    } // end of getEntries function
  };
}); // end of FeedService

app.factory('HttpCachedService', function($http, $q, ConnectivityMonitor, LocalStorage) {
  return {
    getData: function(url, cacheKey) {
      if (ConnectivityMonitor.isOnline()) {
        return $http.get(url).then(function(response) {
          // salva os dados em cache
          LocalStorage.setObject(cacheKey, response.data);
          // retorna os salvos
          return response.data;
        });
      } else {
        var deferred = $q.defer();

        if (LocalStorage.hasKey(cacheKey)) {
          deferred.resolve(LocalStorage.getObject(cacheKey));
        } else {
          deferred.reject('Não é possível acessar a rede');
        }

        return deferred.promise;
      }
    }
  };  
});

app.factory('ComentariosService', function($http, HttpCachedService) {
  return {
    getProvas: function() {
      return HttpCachedService.getData(VIBRACAO_SERVER_BASE_URL + '/api/provas.json', 'cache_comentarios');
    }
  };
});

app.factory('NoticiasService', function($http, HttpCachedService) {
  return {
    getNoticias: function() {
      return HttpCachedService.getData(VIBRACAO_SERVER_BASE_URL + '/api/noticias.json', 'cache_noticias');
    }
  };  
});

app.factory('DicasService', function($http, HttpCachedService) {
  return {
    getDicas: function(area) {
      return HttpCachedService.getData(VIBRACAO_SERVER_BASE_URL + '/api/' + area + '/dicas.json', 'cache_dicas_' + area);
    }
  };  
});

app.factory('CoresService', function($http, HttpCachedService) {
  return {
    getCores: function(area) {
      return HttpCachedService.getData(VIBRACAO_SERVER_BASE_URL + '/api/cores.json', 'cache_cores');
    }
  };  
});

app.factory('AvisosService', function($http, HttpCachedService) {
  return {
    getAvisos: function() {
      return HttpCachedService.getData(VIBRACAO_SERVER_BASE_URL + '/api/avisos.json', 'cache_avisos');
    }
  };  
});

app.factory('GabaritosService', function($http, HttpCachedService) {
  return {
    getGabarito: function(prova_id, cor_id) {
      return HttpCachedService.getData(VIBRACAO_SERVER_BASE_URL + '/api/' + prova_id + '/' + cor_id + '/gabaritos.json', 'cache_gabarito_' + prova_id + '_' + cor_id);
    }
  };  
});

app.factory('VideosService', function($http) {
  var BASE_URL = VIBRACAO_SERVER_BASE_URL + '/api/videos.json';

  return {
    getVideos: function() {
      return $http.get(BASE_URL).then(function(response) {
        return response.data;
      });           
    }
  };  
});

app.factory('FotosService', function($http) {
  var BASE_URL = VIBRACAO_SERVER_BASE_URL + '/api/fotos.json';

  return {
    getFotos: function() {
      return $http.get(BASE_URL).then(function(response) {
        return response.data;
      });           
    }
  };  
});

app.factory('Push', function($resource) {
  return $resource(
    VIBRACAO_SERVER_BASE_URL + "/push?platform=:platform&token=:token", 
    {platform: '@platform', token: '@token'},{
      'subscribe': {method: 'GET', isArray: false, url: VIBRACAO_SERVER_BASE_URL + "/push/subscribe?platform=:platform&token=:token"}
    }
  );
});
